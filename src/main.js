// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import '@fortawesome/fontawesome-free/css/all.css'
// third party
import VueScrollTo from 'vue-scrollto'

import {
  Vuetify,
  VApp,
  VAlert,
  VAutocomplete,
  VCard,
  VNavigationDrawer,
  VFooter,
  VList,
  VBtn,
  VBtnToggle,
  VCheckbox,
  VDatePicker,
  VDivider,
  VDialog,
  VExpansionPanel,
  VIcon,
  VGrid,
  VMenu,
  VRadioGroup,
  VSpeedDial,
  VTabs,
  VTextarea,
  VTextField,
  VToolbar,
  transitions
} from 'vuetify'
import '../node_modules/vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
  components: {
    VApp,
    VAlert,
    VAutocomplete,
    VCard,
    VNavigationDrawer,
    VFooter,
    VList,
    VBtn,
    VBtnToggle,
    VCheckbox,
    VDatePicker,
    VDivider,
    VDialog,
    VExpansionPanel,
    VIcon,
    VGrid,
    VMenu,
    VRadioGroup,
    VSpeedDial,
    VTabs,
    VTextarea,
    VTextField,
    VToolbar,
    transitions
  },
  theme: {
    primary: '#FFFFFF',
    secondary: '#424242',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107'
  },
  iconfont: 'fa'
})
// global
global._ = require('lodash')

Vue.config.productionTip = false

Vue.prototype.$eventHub = new Vue() // Global event bus

Vue.use(VueScrollTo, {
  container: 'body',
  duration: 1000,
  easing: 'ease',
  offset: 0,
  cancelable: false,
  onDone: false,
  onCancel: false,
  x: false,
  y: true
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})
