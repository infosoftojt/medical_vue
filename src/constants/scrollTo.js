export default {
  data: () => ({
    scrollto: [
      {
        el: '#medical-info',
        offset: -115,
        title: 'Medical History'
      },
      {
        el: '#veratans-info',
        offset: -115,
        title: 'Veteran History'
      },
      {
        el: '#diagnostic-info',
        offset: -115,
        title: 'Diagnostic Test/s'
      },
      {
        el: '#surgical-info',
        offset: -115,
        title: 'Surgical Procedures'
      },
      {
        el: '#assistive-info',
        offset: -115,
        title: 'Assistive Devices'
      },
      {
        el: '#initial-range-of-motion-info',
        offset: -115,
        title: 'Initian Range of Motion'
      },
      {
        el: '#joint-stability-info',
        offset: -115,
        title: 'Joint Stability Tests'
      },
      {
        el: '#other-exam-info',
        offset: -115,
        title: 'Other Physical Exam Findings'
      },
      {
        el: '#ankylosis-info',
        offset: -115,
        title: 'Ankylosis'
      },
      {
        el: '#repeated-use-info',
        offset: -115,
        title: 'Repeated Use Overtime'
      },
      {
        el: '#flare-ups-info',
        offset: -115,
        title: 'Flare Ups'
      },
      {
        el: '#additional-info',
        offset: -115,
        title: 'Additional Factors'
      },
      {
        el: '#other-info',
        offset: -115,
        title: 'Other Pertinent PE'
      },
      {
        el: '#remaining-function-info',
        offset: -115,
        title: 'Remaining Effective Function'
      },
      {
        el: '#functional-impact-info',
        offset: -115,
        title: 'Functional Impact'
      },
      {
        el: '#remarks-info',
        offset: -115,
        title: 'Remarks'
      },
      {
        el: '#additional-question-info',
        offset: -115,
        title: 'Additional Questions'
      },
      {
        el: '#rationale-info',
        offset: -115,
        title: 'Rationale'
      }
    ]
  })
}
