export default {
  data: () => ({
    parts: [
      {
        label: 'Amputation',
        value: 'Amputation'
      },
      {
        label: 'Ankle',
        value: 'Ankle'
      },
      {
        label: 'Aid&Attend',
        value: 'Aid&Attend'
      },
      {
        label: 'Bone',
        value: 'Bone'
      },
      {
        label: 'Cspine',
        value: 'Cspine'
      },
      {
        label: 'Endocrine',
        value: 'Endocrine'
      },
      {
        label: 'Elbow',
        value: 'Elbow'
      },
      {
        label: 'Foot',
        value: 'Foot'
      },
      {
        label: 'Hand/Finger',
        value: 'Hand/Finger'
      },
      {
        label: 'Hip Thigh',
        value: 'Hip Thigh'
      },
      {
        label: 'Knee',
        value: 'Knee'
      },
      {
        label: 'Non-Degen Arthritis',
        value: 'Non-Degen Arthritis'
      },
      {
        label: 'Muscle Injury',
        value: 'Muscle Injury'
      },
      {
        label: 'Shoulder',
        value: 'Shoulder'
      },
      {
        label: 'Tmj',
        value: 'Tmj'
      },
      {
        label: 'Tspine',
        value: 'Tspine'
      },
      {
        label: 'Wrist',
        value: 'Wrist'
      },
      {
        label: 'Gen Med Compensation',
        value: 'Gen Med Compensation'
      },
      {
        label: 'Gen Med Gulf War',
        value: 'Gen Med Gulf War'
      },
      {
        label: 'Gen Med Sha',
        value: 'Gen Med Sha'
      },
      {
        label: 'Artery/Vein',
        value: 'Artery/Vein'
      },
      {
        label: 'Breast',
        value: 'Breast'
      },
      {
        label: 'Chronic Fatigue',
        value: 'Chronic Fatigue'
      },
      {
        label: 'Cns',
        value: 'Cns'
      },
      {
        label: 'Cold Injury',
        value: 'Cold Injury'
      },
      {
        label: 'Cranial Nerve',
        value: 'Cranial Nerve'
      },
      {
        label: 'Diabetes',
        value: 'Diabetes'
      },
      {
        label: 'Diabetic Neuropathy',
        value: 'Diabetic Neuropathy'
      },
      {
        label: 'Ear Conditions',
        value: 'Ear Conditions'
      },
      {
        label: 'Hip/Thigh old',
        value: 'Hip/Thigh old'
      },
      {
        label: 'Esophageal',
        value: 'Esophageal'
      },
      {
        label: 'fibromyalgia',
        value: 'fibromyalgia'
      },
      {
        label: 'Gall Bladder/Pancreas',
        value: 'Gall Bladder/Pancreas'
      },
      {
        label: 'Gyno',
        value: 'Gyno'
      },
      {
        label: 'Headache',
        value: 'Headache'
      },
      {
        label: 'Heart',
        value: 'Heart'
      },
      {
        label: 'Hema/Lymphatic',
        value: 'Hema/Lymphatic'
      },
      {
        label: 'Hernia',
        value: 'Hernia'
      },
      {
        label: 'Hypertension',
        value: 'Hypertension'
      },
      {
        label: 'Infectious',
        value: 'Infectious'
      },
      {
        label: 'Intestinal SX',
        value: 'Intestinal SX'
      },
      {
        label: 'Intestines',
        value: 'Intestines'
      },
      {
        label: 'Intestines Infectious',
        value: 'Intestines Infectious'
      },
      {
        label: 'Kidney',
        value: 'Kidney'
      }
    ]
  })
}
