export default {
  methods: {
    goTo (payload) {
      this.$router.push(payload)
    }
  }
}
